package com.example.INTIA.INTIA.repository;

import com.example.INTIA.INTIA.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository   extends JpaRepository<Client,Long> {
}
