package com.example.INTIA.INTIA.service;

import com.example.INTIA.INTIA.entity.Client;

import java.util.List;

public interface ClientService {

    List<Client> getAllClient();
    Client addClient(Client client);
    void  deleteClient(Long id);
    Client updateClient(Long id, Client updatedClient);
    Client getClient(Long id);
}
