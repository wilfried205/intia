package com.example.INTIA.INTIA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntiaApplication.class, args);
	}

}
