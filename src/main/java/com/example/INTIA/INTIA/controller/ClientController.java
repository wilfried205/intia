package com.example.INTIA.INTIA.controller;

import com.example.INTIA.INTIA.entity.Client;
import com.example.INTIA.INTIA.service.ClientService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
@CrossOrigin(origins = "http://localhost:3000")
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    public List<Client> getAllClient(){
    return  clientService.getAllClient();
    }

    @PostMapping
    public Client addClient(@RequestBody Client client) {
        return clientService.addClient(client);
    }

    @DeleteMapping("/{id}")
    public void  deleteClient(@PathVariable Long id){
        clientService.deleteClient(id);
    }
}
